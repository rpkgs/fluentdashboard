// eslint-disable-next-line no-unused-vars
const webpack = require('webpack');
const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

const config = {
  mode: 'production',
  entry: './src/lib/index.bundle.js',
  resolve: {
    extensions: [
      '.tsx',
      '.ts',
      '.jsx',
      '.js'
    ]
  },
  output: {
    path: path.join(__dirname, '..', 'inst', 'assets', "js"),
    filename: 'fluentDashboard-react.js',
  },
  module: {
    rules: [{
        test: /\.(js|jsx)$/,
        use: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.ts(x)?$/,
        loader: 'ts-loader',
        exclude: /node_modules/
      }
    ],
  },
  externals: {
    'react': 'jsmodule["react"]',
    'react-dom': 'jsmodule["react-dom"]',
    '@/shiny.react': 'jsmodule["@/shiny.react"]'
  },
  performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000,
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({
      terserOptions: {
        compress: {
            drop_console: true
        }
      }
    })],
  },
};

module.exports = config;