
var toogleSideNav = function() {
  var grid = document.querySelector('.grid-container')
  if (document.getElementById('sidenav').style.display === 'none') {
    document.getElementById('sidenav').style.display = 'block'
    grid.classList.remove('grid-h')
    grid.classList.add('grid-hn')
  } else {
    document.getElementById('sidenav').style.display = 'none';
    grid.classList.remove('grid-hn')
    grid.classList.add('grid-h')
  }
}

document.addEventListener('DOMContentLoaded', function(event) {

  elCollapse = document.getElementById("collapse-menu")
  if (elCollapse) {
    elCollapse.addEventListener("click", toogleSideNav);
  }

})
