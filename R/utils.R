`%then%` <- function(a, b) {
  if (is.null(a)) {
    NULL
  } else {
    b
  }
}


`%or%` <- function(a, b) {
  if (isTRUE(a)) {
    return(TRUE)
  }
  if (isTRUE(b)) {
    return(TRUE)
  }
  if (!is.null(a)) {
    return(TRUE)
  }
  if (!is.null(b)) {
    return(TRUE)
  }
  if (!is.na(a)) {
    return(TRUE)
  }
  if (!is.na(b)) {
    return(TRUE)
  }
  FALSE
}

#' Include FluentDashboard Dependencies
#'
#' @importFrom htmltools htmlDependency
#' @importFrom utils packageVersion
#'
#' @export
use_fd_deps <- function() {
  htmlDependency(
    name = "fluentDashboard",
    package = "fluentDashboard",
    version = packageVersion("fluentDashboard"),
    src = "assets",
    stylesheet = "css/styles.css",
    script = c(
      "js/main.js",
      "js/js-polyfill-object.fromentries/index.js"
    )
  )
}
