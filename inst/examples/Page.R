if (interactive()) {
  ## ---- libraries ----
  library(fluentDashboard)
  library(shiny)
  library(shiny.fluent)
  library(shinipsum)

  ## ---- single-page ----
  single_page <- Page(
    title = "Single-Page",
    subtitle = "Example",
    standalone = TRUE,
    Stack(
      horizontal = TRUE,
      Card(title = "Text 1", textOutput("text1")),
      Card(title = "Text 2", textOutput("text2"))
    ),
    Stack(
      Card(title = "Text 3", textOutput("text3")),
      Card(title = "Text 4", textOutput("text4"))
    )
  )

  ## ---- shiny-app ----
  shinyApp(
    ui = single_page,
    server = function(input, output) {
      output$text1 <- renderText({
        random_text(nwords = 50)
      })
      output$text2 <- renderText({
        random_text(nwords = 50)
      })
      output$text3 <- renderText({
        random_text(nwords = 100)
      })
      output$text4 <- renderText({
        random_text(nwords = 100)
      })
    }
  )
}
