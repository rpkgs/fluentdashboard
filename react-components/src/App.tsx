import React from 'react';
import { Stack, IStackTokens, IStackStyles } from '@fluentui/react';
import './App.css';
import { initializeIcons } from '@fluentui/font-icons-mdl2';
import { useState } from 'react';
import { Autocomplete } from './lib/autocomplete/autocomplete';
import { IItem } from './lib/autocomplete/autocomplete.types';
initializeIcons();

const stackTokens: IStackTokens = { childrenGap: 15 };
const stackStyles: Partial<IStackStyles> = {
  root: {
    margin: '0 auto',
    textAlign: 'center',
    color: '#605e5c',
    justifyContent: 'none'
  },
};

const defaultItems: IItem[] = [
  { key: "1", displayValue: 'Apple', searchValue: 'Apple' },
  { key: "2", displayValue: 'Banana', searchValue: 'Banana' },
  { key: "3", displayValue: 'Orange', searchValue: 'Orange' },
  { key: "4", displayValue: 'Pineapple', searchValue: 'Pineapple' },
  { key: "5", displayValue: 'Pumpkin', searchValue: 'Pumpkin' },
]

//
const itemsServerside: IItem[] = [
  { key: "1", displayValue: 'Apple' },
  { key: "2", displayValue: 'Banana' },
  { key: "3", displayValue: 'Orange' },
  { key: "4", displayValue: 'Pineapple' },
  { key: "5", displayValue: 'Pumpkin' },
]


export const App: React.FunctionComponent = () => {
  const [items, setItems] = useState(defaultItems)
  // const [items, setItems] = useState([] as IItem[])
  console.log(items)

  return (
    <>
      <Stack horizontalAlign="center" verticalAlign="center" verticalFill styles={stackStyles} tokens={stackTokens}>
        <Autocomplete
          serversideFiltering={false}
          items={items}
          onChange={(value: string) => {
            console.log(`App/onChange(${value})`)
            // setItems(
            //   itemsServerside.filter(item => {
            //   return item.displayValue.toLowerCase().indexOf(value.toLowerCase()) >= 0
            // }))
          }}
          onSelect={(value: string) => console.log(`App/onSelect(${value})`)}
          onSearch={(value: string) => console.log(`App/onSearch(${value})`)}
          placeholder="Search"
        />
      </Stack>
      <Stack>

        {[...Array(10).keys()].map(i => (
          <span key={i}>{i}</span>
        ))}

      </Stack>
    </>
  );
};
